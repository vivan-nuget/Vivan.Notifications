# Vivan.Notifications

Biblioteca que possui um conjunto de recursos para implementação de um notification pattern facilitando o desenvolvimento das aplicações 

## Instalação

Instalar o package **_'Vivan.Notifications'_** através do Nuget Package Manager (Servidor da Vivan).

## Dependências


## Recursos
* NotificationContext
* Classe Notification
* Classe Validatable


## Utilizando os recursos do NotificationContext

O serviço **NotificationContext** tem como finalidade o armazenamento de notificações geradas no domínio da aplicação, garantindo assim o retorno de informações consistentes e legíveis para os usuários:

Para utilizar o NotificationContext, é necessário registrar a interface INotificationContext no container do .net core (método ConfigureServices):

```c#
// This method gets called by the runtime. Use this method to add services to the container.
public void ConfigureServices(IServiceCollection services)
{
    services.AddScoped<INotificationContext, NotificationContext>();

    ...
}
```

Após registrá-lo no container, basta injetar a interface nos serviços e utilizá-lo:

```c#
    public class CatalogoDeProdutoService
    {
        public readonly INotificationContext _notificationContext;

        //Injetando o 
        public CatalogoDeProdutoService(INotificationContext notificationContext)
        {
            _notificationContext = notificationContext;
        }

        //Adicionando Notificações no Notification Context
        public void AdicionandoNotificacoesNoNotificationContext()
        {
            _notificationContext.AddNotification("Adicionando uma notificação");

            var validationResults = new UmAbstractValidatorQualquer(UmaVariavelQualquer);

            _notificationContext.AddNotification(validationResults);
        }

        //Acessando Notificações do Notification Context
        public void AcessandoNotificacoesDoNotificationContext()
        {
            if (_notificationContext.HasNotifications)
            {
                var notificacoes = _notificationContext.Notifications();
            }
        }
    }
```

## Utilizando os recursos da classe Notification

A classe **Notification** é a entidade que o NotificationContext armazena e gerencia em seu escopo.

Elas pode ser instanciadas e adicionadas no notification context.

```c#
public void CriandoNotificacoes()
{
    var notificacao1 = new Notification("Estamos criando uma notificação sem chave.");
    var notificacao2 = new Notification("chave da notificacao", "Estamos criando uma notificação com chave.");
    
    _notificationContext.AddNotification(notificacao1);
    _notificationContext.AddNotification(notificacao2);
}
```

## Utilizando os recursos da classe Notification

A classe **Validatable** é responsável por tornar qualquer classe Notificável e validável.

Classes que herdam **Validatable**, possuem atributos que dizem se elas estão validas ou não, armazenam notificações e possuem um metodo protected chamado UseValidator que permite a execução AbstractValidators.

Considere a seguinte classe Catalogo:

```c#
public class Catalogo : Validatable
{
    public int Id { get; set; }

    public string Nome { get; set;}

    public void Atualizar(string nome)
    {
        Nome = nome;
    }

    public override bool Validate() //Método abstract da classe Validatable
    {
        UseValidator(this, new CatalogValidator()); //Valida se possui o nome
    }
}
```

```c#
public void TestandoSeUmaEntidadeEstaValida()
{
    var catalogo = await _catalogoRepository.ObterPorId(model.Id);

    catalogo.AtualizarNome(null);

    if (catalogo.IsValid)
        _catalogoRepository.Atualizar(catalogo);
    else
        _notificationContext.AddNotifications(catalogo);
}
```
