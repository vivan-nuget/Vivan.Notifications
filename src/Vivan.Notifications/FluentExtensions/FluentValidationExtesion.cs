﻿using FluentValidation;
using FluentValidation.Validators;
using Vivan.Notifications.Validations;

namespace Vivan.Notifications.FluentExtensions
{
    public static class FluentValidationExtesion
    {
        public static IRuleBuilderInitial<T, TProperty> IsValid<T, TProperty>(this IRuleBuilder<T, TProperty> ruleBuilder) where TProperty : Validatable
        {
            return ruleBuilder.Custom(IsValid);
        }

        private static void IsValid<T>(T validatable, CustomContext context) where T : Validatable
        {
            if (validatable == default(T))
                return;

            if (validatable.IsInvalid)
            {
                foreach (var failure in validatable.ValidationResult.Errors)
                {
                    context.AddFailure(failure);
                }
            }
        }
    }
}
