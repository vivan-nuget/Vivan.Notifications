﻿using FluentValidation.Results;
using System.Collections.Generic;
using Vivan.Notifications.Entities;
using Vivan.Notifications.Validations;

namespace Vivan.Notifications.Interfaces
{
    public interface INotificationContext
    {
        IReadOnlyCollection<Notification> Notifications { get; }

        bool HasNotifications { get; }

        void AddNotification(string message);

        void AddNotification(string key, string message);

        void AddNotification(Notification notification);

        void AddNotifications(IReadOnlyCollection<Notification> notifications);

        void AddNotifications(IList<Notification> notifications);

        void AddNotifications(ICollection<Notification> notifications);

        void AddNotifications(IEnumerable<Notification> notifications);

        void AddNotifications(ICollection<string> notifications);

        void AddNotifications(IEnumerable<string> notifications);

        void AddNotifications(Validatable validatable);

        void AddNotifications(ValidationResult validationResult);
    }
}
