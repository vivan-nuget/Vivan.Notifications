﻿namespace Vivan.Notifications.Entities
{
    public class Notification
    {
        public string Key { get; }

        public string Message { get; }

        public Notification(string message)
        {
            Key = string.Empty;
            Message = message;
        }

        public Notification(string key, string message)
        {
            Key = key;
            Message = message;
        }
    }
}
