﻿using FluentValidation;
using FluentValidation.Results;
using System;
using System.Linq;

namespace Vivan.Notifications.Validations
{
    public abstract class Validatable
    {
        protected Validatable()
        {
            ValidationResult = new ValidationResult();
        }

        public ValidationResult ValidationResult { get; private set; }

        public bool IsValid => Validate();

        public bool IsInvalid => !IsValid;

        public virtual bool Validate()
        {
            return ValidationResult.IsValid;
        }

        protected bool UseValidator<TModel>(TModel model, AbstractValidator<TModel> validator)
        {
            var result = validator.Validate(model);

            if (!result.IsValid)
                AddNotifications(result);

            return ValidationResult.IsValid;
        }

        protected bool UseValidator<TValidator>(object model) where TValidator : IValidator
        {
            var validator = (TValidator)Activator.CreateInstance(typeof(TValidator));

            var result = validator.Validate(model);

            return result.IsValid;
        }

        public void AddNotification(string errorMessage) => AddNotification(string.Empty, errorMessage);

        protected void AddNotifications(ValidationResult validationResult)
        {
            foreach (var failure in validationResult.Errors)
            {
                if (!ValidationResult.Errors.Any(f => f.PropertyName == failure.PropertyName
                                                   && f.ErrorMessage == failure.ErrorMessage))
                {
                    ValidationResult.Errors.Add(failure);
                }
            }
        }

        public void AddNotification(string propertyName, string errorMessage)
        {
            ValidationResult.Errors.Add(new ValidationFailure(propertyName, errorMessage));
        }
    }
}
