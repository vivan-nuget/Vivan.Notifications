﻿using Vivan.Notifications.Validations;

namespace Vivan.Notifications.Extensions
{
    public static class ValidatableExtension
    {
        public static bool IsNullOrInvalid<TValidatable>(this TValidatable validatable) where TValidatable : Validatable
        {
            if (validatable == null)
                return true;

            return validatable.IsInvalid;
        }
    }
}
